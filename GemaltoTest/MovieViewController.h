//
//  ViewController.h
//  GemaltoTest
//
//  Created by Atul Kumar on 6/14/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property(nonatomic,weak) IBOutlet UITableView *tblMovie;
@property(nonatomic,weak) IBOutlet UISearchBar *searchBar;

@end

