//
//  ViewController.m
//  GemaltoTest
//
//  Created by Atul Kumar on 6/14/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#import "MovieViewController.h"
#import <DynamicFrameWork/APIRequest.h>
#import "MovieTableViewCell.h"

@interface MovieViewController ()<APIRequestDelegate> {
    NSArray *_movieList;
}

@end

@implementation MovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [APIRequest getInstance].delegate = self;
    [[APIRequest getInstance]apiCallForDiscoverMovie];
    
}
// Mark -
// search bar delegate
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
        [[APIRequest getInstance] apiCallForDiscoverMovie];
    }
    else {
        [[APIRequest getInstance] getSearchMovie:searchText];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
// Mark -
// table view delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_movieList count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *celldentifier = @"movieCell";
    MovieTableViewCell *movieCell = [tableView dequeueReusableCellWithIdentifier:celldentifier];
    if (!movieCell) {
        movieCell = [[MovieTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:celldentifier];
    }
    [movieCell bind:[_movieList objectAtIndex:indexPath.row]];
    return movieCell;
}

// api callback
- (void)getMovieApiCallback:(NSArray *)movieList {
    _movieList = movieList;
    [self.tblMovie reloadData];
}


@end
