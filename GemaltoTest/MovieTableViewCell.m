
#import "MovieTableViewCell.h"

@implementation MovieTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)bind:(NSDictionary*)dict {
    self.lblMovieTitle.text = [dict valueForKey:@"title"];
    self.lblRating.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"vote_average"]];

}

@end
