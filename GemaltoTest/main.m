//
//  main.m
//  GemaltoTest
//
//  Created by Atul Kumar on 6/14/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
