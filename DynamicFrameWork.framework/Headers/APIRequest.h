//
//  APIRequest.h
//  DynamicFrameWork
//
//  Created by Atul Kumar on 6/14/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol APIRequestDelegate <NSObject>
-(void)getMovieApiCallback:(NSArray*)movieList;
@end

@interface APIRequest : NSObject
+(APIRequest*)getInstance;
@property (nonatomic,strong) id<APIRequestDelegate> delegate;
-(void)apiCallForDiscoverMovie;
-(void)getSearchMovie:(NSString*)text;
@end
NS_ASSUME_NONNULL_END
